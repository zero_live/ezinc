
TranslateZone = {
    setUp = function(self)
        local translationService = TranslationsSevice:new()
        local action = TranslateZone:_new(translationService)
        return action
    end,

    _new = function(self, translationService)
        local instance = {
            _translationService = translationService,
        }
        setmetatable(instance, { __index = self })
        return instance
    end,

    action = function(self, zoneToTranslate)
        local label = self:_toLabel(zoneToTranslate)

        local translation = self._translationService:zoneName(label)

        return translation
    end,

    _toLabel = function(self, zoneToTranslate)
        local trimmedZone = string.gsub(zoneToTranslate, "%s+", "")
        local label = string.lower(trimmedZone)

        return label
    end,
}
