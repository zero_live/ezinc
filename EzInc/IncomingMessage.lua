
IncomingMessage = {
    _INCOMING_MESSAGE = "%s incoming to %s",
    _message = nil,
    _enemiesQuantity = nil,
    _zoneName = nil,
    builderFor = function(self, enemiesQuantity)
        self._enemiesQuantity = enemiesQuantity
        return self
    end,
    enemiesSeenAttacking = function(self)
        self._message = self._INCOMING_MESSAGE
        return self
    end,
    zone = function(self, zoneName)
        self._zoneName = zoneName
        return self
    end,
    build = function(self)
        local upperZoneName = string.upper(self._zoneName)

        return string.format(self._message, self._enemiesQuantity, upperZoneName)
    end,
}
