# EzInc

WoW Addon to notify your BG-Mates when the base you are defending is under attack (always in English). Check supported languages.

# Production Environment

## System requirementes

- World of Warcraft Dragonfligth or compatibles.

## How to use the addon

1º Download the [addon](https://gitlab.com/zero_live/ezinc/-/jobs/artifacts/master/download?job=build_job)

2º Unzip the content inside your wow folder at: ´_retail_\Interface\AddOns\EzInc\´

3º Reload or lunch WoW

> When you log with your character you can see a litle buttons in the left bottom corner of the screen that are usables in supported BGs.

### Current Supported languajes

- Spanish
- English

### Current Supported BGs

- Arathi Basin
- Eye of the Storm
- Battle for Gilneas

# Test environment

## System requirementes

- Docker version 24.0.6 or compatible.
- Docker Compose version v2.22.0 or compatible.
- Bash terminal or compatible.

## How to run the test suit

1º Build the proyect with: `sh scripts/up.sh`

2º Run the test suit with: `sh scripts/run-tests.sh`

> Once you end your work, remember to take down the proyect for avoid future problems. You can use `sh scripts/down.sh`

# Feedback

All feedback is welcome ^^ you can send me a mail to zerolive@gmail.com
