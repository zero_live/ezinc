FROM debian:12.5-slim
WORKDIR /opt/EzInc

RUN apt-get update && \
    apt-get install -y lua5.3 lua-sec lua5.3-dev luarocks && \
    luarocks install luaunit

COPY . /opt/EzInc
