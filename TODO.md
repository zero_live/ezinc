
# TO-DO

[x] Logo
[ ] What do developers write in Addons README?
[x] Update README
    [x] How to give feedback.
    [x] How to install.
[x] Pusblish addon in CurseForge
    [x] Wait for revision
[x] What do I need to publish the addon in Curse?
[x] Pipeline create artifact
[x] Pipeline for run test before create artifact
[x] Test current behavior
[x] Translate spanish zones to english
    [x] Add english zones for bases BG
    [x] Translate spanish zones to english
    [x] Update readme with supported zones
[ ] Open Issues for translations
    [ ] What translations are needed
[ ] Tech deb
    [x] Integration test for every button
    [ ] Create test setup for clean environment
    [ ] Move test helpers to its own files
    [x] Use instance variable in TranslateZone action
    [x] Create Message builder
    [ ] Create callInc action?
