
function currentZoneName()
    local zoneName = GetMinimapZoneText()

    return zoneName
end

function callIncomingFor(quantity)
    local translatedZone = TranslateZone:setUp():action(currentZoneName())

    if translatedZone then
        local incomingMessage = IncomingMessage:builderFor(quantity):enemiesSeenAttacking():zone(translatedZone):build()

        SendChatMessage(incomingMessage, "INSTANCE_CHAT")
    end
end

local MoreThanThreeInc = CreateFrame("Button", "MoreThanThreeInc", UIParent, "UIPanelButtonTemplate")
MoreThanThreeInc:SetSize(22 ,22)
MoreThanThreeInc:SetText("+")
MoreThanThreeInc:SetPoint("BOTTOMLEFT", 0, 0)
MoreThanThreeInc:SetScript("OnClick", function()
    callIncomingFor("MORE THAN THREE")
end)

local ThreeInc = CreateFrame("Button", "ThreeInc", UIParent, "UIPanelButtonTemplate")
ThreeInc:SetSize(22 ,22)
ThreeInc:SetText("3")
ThreeInc:SetPoint("BOTTOMLEFT", 22, 0)
ThreeInc:SetScript("OnClick", function()
    callIncomingFor("THREE")
end)

local TwoInc = CreateFrame("Button", "TwoInc", UIParent, "UIPanelButtonTemplate")
TwoInc:SetSize(22 ,22)
TwoInc:SetText("2")
TwoInc:SetPoint("BOTTOMLEFT", 44, 0)
TwoInc:SetScript("OnClick", function()
    callIncomingFor("TWO")
end)

local OneInc = CreateFrame("Button", "OneInc", UIParent, "UIPanelButtonTemplate")
OneInc:SetSize(22 ,22)
OneInc:SetText("1")
OneInc:SetPoint("BOTTOMLEFT", 66, 0)
OneInc:SetScript("OnClick", function()
    callIncomingFor("ONE")
end)
