require("../EzInc/TranslateZone")

luaunit = require("luaunit")

require("../tests/TestEzInc")
require("../tests/EzInc/TestTranslationsService")
require("../tests/EzInc/TestTranslateZone")

luaunit.run()
