require("../../EzInc/IncomingMessage")

TestTranslateZone = {}

function TestTranslateZone:testTranslatesSpanishZoneToEnlish()
    local spanishZone = 'Ruinas del Atracador Vil'
    local englishZone = 'Fel Reaver Ruins'

    local translatedZone = TranslateZone:setUp():action(spanishZone)

    luaunit.assertEquals(translatedZone, englishZone)
end
