require("../../EzInc/TranslationsSevice")

TestTranslationsService = {}

function TestTranslationsService:testTranslatesEnglishZoneNameToEnglish()
    local zoneNameInEnglish = "Blood Elf Tower"
    local label = "bloodelftower"

    local translation = TranslationsSevice:new():zoneName(label)

    luaunit.assertEquals(translation, zoneNameInEnglish)
end

function TestTranslationsService:testTranslatesSpanishZoneNameToEnglish()
    local zoneNameInEnglish = "Blacksmith"
    local label = "herrería"

    local translation = TranslationsSevice:new():zoneName(label)

    luaunit.assertEquals(translation, zoneNameInEnglish)
end
