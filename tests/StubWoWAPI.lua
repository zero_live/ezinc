lastSendedMessageToAnyChat = nil
lastMessageSendedToContext = nil
minimapZoneText = "NO ZONE"

WoWFrames = {
    new = function(self)
        local instance = {
            _frames = {},
        }
        setmetatable(instance, { __index = self })
        return instance
    end,
    addCreatedFrame = function(self, id, frame)
        self._frames[id] = frame
    end,
    clickOn = function(self, frameId)
        self._frames[frameId]:click()
    end,
}

wowFrames = WoWFrames:new()

function SendChatMessage(message, context)
    lastSendedMessageToAnyChat = message
    lastMessageSendedToContext = context
end

function setMinimapZoneText(zoneName)
    minimapZoneText = zoneName
end

function GetMinimapZoneText()
    return minimapZoneText
end

Frame = {
    create = function(self, name)
        local instance = {
            _name = name,
            _eventsCallback = {},
        }
        setmetatable(instance, { __index = self })
        return instance
    end,
    SetSize = function(self, height, width)

    end,
    SetText = function(self, text)

    end,
    SetPoint = function(self, initialPosition, pixelsToLeft, pixelsToRigth)

    end,
    SetScript = function(self, event, callback)
        self._eventsCallback[event] = callback
    end,
    click = function(self)
        self._eventsCallback["OnClick"]()
    end
}

function CreateFrame(type, name, uiParent, template)
    local frame = Frame:create(name)
    wowFrames:addCreatedFrame(name, frame)
    return frame
end
