require("../tests/StubWoWAPI")

function MovePlayerTo(zoneName)
    setMinimapZoneText(zoneName)
end

function expectMessageInInsteanceGroup(message)
    luaunit.assertEquals(message, lastSendedMessageToAnyChat)
    luaunit.assertEquals(lastMessageSendedToContext, "INSTANCE_CHAT")
end

require("../EzInc")

TestEzInc = {}

function TestEzInc:testOnClickMoreThenThreeButtonCallIncInInstanceGroup()
    MovePlayerTo("Aserradero")

    wowFrames:clickOn("MoreThanThreeInc")

    expectMessageInInsteanceGroup("MORE THAN THREE incoming to LUMBER MILL")
end

function TestEzInc:testOnClickThreeButtonCallIncInInstanceGroup()
    MovePlayerTo("Waterworks")

    wowFrames:clickOn("ThreeInc")

    expectMessageInInsteanceGroup("THREE incoming to WATERWORKS")
end

function TestEzInc:testOnClickTwoButtonCallIncInInstanceGroup()
    MovePlayerTo("Torre de los Elfos de Sangre")

    wowFrames:clickOn("TwoInc")

    expectMessageInInsteanceGroup("TWO incoming to BLOOD ELF TOWER")
end

function TestEzInc:testOnClickTwoButtonCallIncInInstanceGroup()
    MovePlayerTo("Establo")

    wowFrames:clickOn("OneInc")

    expectMessageInInsteanceGroup("ONE incoming to STABLES")
end
