## Interface: 100205
## Version: 1.0.3
## Title: EzInc
## Notes: Easy Inc caller
## Author: ZeroLive
## IconTexture: Interface\AddOns\ezinc\media\icon

EzInc\TranslationsSevice.lua
EzInc\IncomingMessage.lua
EzInc\TranslateZone.lua
EzInc.lua
