#/bin/sh

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source "$SCRIPT_DIR/../.env"

latest_version=$(git describe --tags --abbrev=0)
zip_file="EzInc_$latest_version.zip"

curl -o "$zip_file" -LO https://gitlab.com/zero_live/ezinc/-/jobs/artifacts/master/download?job=build_job
curl -X POST -H "X-API-Token: $API_TOKEN" -F "file=@$zip_file" -F "metadata={'displayName':'$zip_file','changelog':'$latest_version','gameVersions':[10845],'releaseType':'release','changeDescription':'$latest_version'}" https://wow.curseforge.com/api/projects/$ID_DEL_PROYECTO/upload-file
