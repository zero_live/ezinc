#/bin/sh

mkdir ezinc &&
cp ./README.md ./ezinc/ &&
cp ./EzInc.lua ./ezinc/ &&
cp ./EzInc.toc ./ezinc/ &&
cp ./LICENSE ./ezinc/ &&
mkdir ./ezinc/EzInc/ &&
cp -r ./EzInc/** ./ezinc/EzInc
